
*** Settings ***
Library     SeleniumLibrary
Resource    StepDefinition/billetsteps.resource
Resource    StepDefinition/setup_teardown.resource
Resource    StepDefinition/commonsteps.resource
Test Setup    ${setup}
Test Teardown    ${teardown}


*** Test Cases ***
Ajout de tickets plein Tarif 
    Given je suis sur la page d'accueil du musée
    When je navigue dans la billetterie jusqu'à l'achat de billet d'entrée au musée
    Then je peux ajouter "${JDD1}[1]" billet adulte plein tarif     
    And ma selection affiche bien " ${JDD1}[1]" billet     

Ajout de plusieurs ticket plein Tarif
    Given je suis sur la page d'accueil du musée
    When je navigue dans la billetterie jusqu'à l'achat de billet d'entrée au musée
    Then je peux ajouter "${JDD2}[famille]" billet adulte plein tarif
    And ma selection affiche bien "${JDD2}[famille]" billet

Ajout de 1 ticket Adulte plein Tarif et 1 billet nocturne
    Given je suis sur la page d'accueil du musée
    When je navigue dans la billetterie jusqu'à l'achat de billet d'entrée au musée
    Then je peux ajouter "1" billet(s) adulte plein tarif et "1" billet(s) nocturne  
    And ma selection affiche bien "1" billets adulte plein tarif et "1" billets nocturne

Ajout de 2 billets Adulte plein Tarif et 6 billets nocturne
    Given je suis sur la page d'accueil du musée
    When je navigue dans la billetterie jusqu'à l'achat de billet d'entrée au musée
    Then je peux ajouter "2" billet(s) adulte plein tarif et "5" billet(s) nocturne  
    And ma selection affiche bien "2" billets adulte plein tarif et "5" billets nocturne