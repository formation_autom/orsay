
*** Settings ***
Library     SeleniumLibrary
Resource    StepDefinition/visitesteps.resource
Resource    StepDefinition/setup_teardown.resource
Resource    StepDefinition/commonsteps.resource
Test Setup    ${setup}
Test Teardown    ${teardown}


*** Test Cases ***
Informations visite guidee - individuel

	Given je suis sur la page d'accueil du musée
	When je clique sur l'onglet Vous êtes
	And je choisis le profil adéquat    ${JDDind}[1]
	Then j'obtiens des informations sur les visites guidées    ${JDDind}[2]

Informations visite guidee - famille

	Given je suis sur la page d'accueil du musée
	When je clique sur l'onglet Vous êtes
	And je choisis le profil adéquat    ${JDDfam}[1]
	Then j'obtiens des informations sur les visites guidées    ${JDDfam}[2]
